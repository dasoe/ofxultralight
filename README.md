## ofxUltralight

openframeworks 0.11.0 addon for ultralight-sdk-1.2-win https://ultralig.ht/ 
Ultralight (c) 2018 Ultralight, Inc. Ultralight is a trademark of Ultralight, Inc.

# as this is a openFrameworks addon, I moved the repository.
### find the actual version at the [other git repo](https://github.com/dasoe/ofxUltralight)
